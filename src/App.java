import com.devcamp.bookauthor.models.Author;
import com.devcamp.bookauthor.models.Book;

public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Chung Chi Bao Son","sonchung@gmail.com", 'm');
        System.out.println("Author1:");
        System.out.println(author1);
        
        Author author2 = new Author("Quynh Nga","quynhnga@gmail.com", 'f');
        System.out.println("Author2:");
        System.out.println(author2);

        Book book1 = new Book("Java", author1, 30000);
        System.out.println("Book1:");
        System.out.println(book1);

        Book book2 = new Book("ReactJs", author1, 50000, 100);
        System.out.println("Book2:");
        System.out.println(book2);
    }
}
